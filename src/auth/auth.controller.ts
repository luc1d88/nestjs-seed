import { Post, Controller, Body, UsePipes, ValidationPipe, HttpException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Token } from './interfaces/Token';
import { LogInDto } from './dto/log-in.dto';
import { ApiUseTags, ApiResponse, ApiOperation, ApiProduces } from '@nestjs/swagger';
import { CreateUserDto } from '../users/dto/create-user.dto';

@ApiUseTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @ApiOperation({ title: 'Login service', description: 'Used to login to application'})
  @ApiResponse({ status: 201, description: 'User has logged in succesfully. Returns authentication token'})
  @ApiResponse({ status: 404, description: 'User or password is wrong.'})
  @ApiResponse({ status: 400, description: 'Input validation failed'})
  @UsePipes(new ValidationPipe())
  async logIn(@Body() login: LogInDto): Promise<Token> {
    let token: Token;
    try {
      token = await this.authService.logIn(login);
    } catch (error) {
      throw new HttpException(error.message, 404);
    }

    return token;
  }

  @Post('signup')
  @ApiOperation({ title: 'Signup service', description: 'Used to signup to application'})
  @ApiResponse({ status: 201, description: 'User has been created in succesfully. Returns authentication token'})
  @ApiResponse({ status: 404, description: 'User already exists.'})
  @ApiResponse({ status: 400, description: 'Input validation failed'})
  @UsePipes(new ValidationPipe())
  async signUp(@Body() signup: CreateUserDto): Promise<Token> {
    let token: Token;
    try {
      token = await this.authService.signUp(signup);
    } catch (error) {
      throw new HttpException(error.message, 404);
    }

    return token;
  }

}
