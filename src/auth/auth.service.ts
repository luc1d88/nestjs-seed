import { Injectable, UnauthorizedException, HttpException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { classToPlain } from 'class-transformer';
import { Token } from './interfaces/Token';
import { UserService } from '../users/user.service';
import { User } from '../users/interface/User';
import { LogInDto } from './dto/log-in.dto';
import { UserSimpleDto } from '../users/dto/user-simple.dto';
import { CreateUserDto } from '../users/dto/create-user.dts';

@Injectable()
export class AuthService {

  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService) {}

  async logIn(loginDto: LogInDto): Promise<Token>{
    const user: User = await this.userService.findOneByEmail(loginDto.email);
    if (!user || !user.validatePass(loginDto.password)) {
      throw new Error('Username or password is wrong !');
    }
    const token: Token = this.createToken(user);

    return token;
  }

  async signUp(createUserDto: CreateUserDto): Promise<Token> {
    const user = await this.userService.create(createUserDto);
    const token: Token = this.createToken(user);
    return token;
  }

  async validate(token: User): Promise<User> {
    const user = await this.userService.findOneByEmail(token.email);

    return user;
  }

  createToken(user: User) {
    const userDto: UserSimpleDto = this.userService.convertUserToSimple(user);
    const userDtoObj: any = classToPlain(userDto);
    const token: Token = {
      accessToken: this.jwtService.sign(userDtoObj, { expiresIn: '30 days'}),
      expiresIn: Math.floor(Date.now() + 1000 * 60 * 60 * 24 * 30),
      created: Date.now(),
    };

    return token;
  }

}
