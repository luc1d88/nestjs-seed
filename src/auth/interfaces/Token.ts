export interface Token {
  expiresIn: number;
  created: number;
  accessToken: string;
}