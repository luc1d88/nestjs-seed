import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from './auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Token } from './interfaces/Token';
import { User } from '../users/interface/User';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'key',
    });
  }

  async validate(token: User): Promise<User> {
    const user = await this.authService.validate(token);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}