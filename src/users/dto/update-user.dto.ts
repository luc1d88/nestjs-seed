import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateUserDto{
  @ApiModelProperty({required: false})
  @IsOptional()
  @IsString()
  readonly password: string;

  @ApiModelProperty({required: false})
  @IsOptional()
  @IsString()
  readonly firstName: string;

  @ApiModelProperty({required: false})
  @IsOptional()
  @IsString()
  readonly lastName: string;
}