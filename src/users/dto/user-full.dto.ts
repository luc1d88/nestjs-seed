import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserFullDto{
  @ApiModelProperty()
  public _id: string;

  @ApiModelProperty()
  public email: string;

  @ApiModelProperty()
  public firstName: string;

  @ApiModelProperty()
  public lastName: string;
}