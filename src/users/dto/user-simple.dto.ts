import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserSimpleDto{

  @ApiModelProperty()
  public _id: string;

  @ApiModelProperty()
  public email: string;

}