import { Document } from 'mongoose';

export interface User extends Document {
  email: string;
  password: string;
  firstName: string;
  lastName: string;

  validatePass(password: string): boolean;
  hashPass(): string;
}
