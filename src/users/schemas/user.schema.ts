import * as mongoose from 'mongoose';
import { SHA256 } from 'crypto-js';

import { User } from '../interface/User';

export const UserSchema = new mongoose.Schema({
  email: { type: String, required: true },
  password: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  created: { type: Date, default: Date.now() },
});

UserSchema.pre('save', function(next) {
  const user: User = this as User;
  user.password = SHA256(user.password).toString();
  next();
});

UserSchema.methods.validatePass = function(password) {
  if (this.password !== SHA256(password).toString())
    return false;
  return true;
};
