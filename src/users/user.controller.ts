import {
  Controller,
  Body,
  Get,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Query,
  Param,
  Put,
  Delete,
  Req,
  HttpException} from '@nestjs/common';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserService } from './user.service';
import { UserSimpleDto } from './dto/user-simple.dto';
import { AuthGuard } from '@nestjs/passport';
import { User } from './interface/User';
import {
  ApiBearerAuth,
  ApiUseTags,
  ApiImplicitQuery,
  ApiImplicitParam,
  ApiOperation,
  ApiResponse,
  ApiOkResponse,
  ApiForbiddenResponse } from '@nestjs/swagger';
import { UserFullDto } from './dto/user-full.dto';

@ApiUseTags('users')
@ApiBearerAuth()
@Controller('users')
export class UserController {

  constructor(
    private readonly userService: UserService) {}

  /*
   * Find all users or filter by email
   */

  @ApiImplicitQuery({
    name: 'email',
    description: 'Search by partial email',
    required: false } )
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({
    title: 'Fetch all users',
    description: 'Can be used to fetch all users from the database. Returns simple data set of the users'} )
  @ApiOkResponse({ description: 'Returns array of users'} )
  @ApiResponse({ status: 401, description: 'Unauthorized'} )
  @UsePipes(new ValidationPipe())
  @Get()
  async findAll(@Query('email') email) {
    let users: User[];
    if (email) {
      users = await this.userService.findAllByEmail(email);
      return users.map((user) => this.userService.convertUserToSimple(user));
    }
    users = await this.userService.findAll();
    return users.map((user) => this.userService.convertUserToSimple(user));
  }

  /*
   * Find user by id
   */

  @ApiImplicitParam({
    name: 'id',
    description: 'Uid for the user',
    required: true })
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({
    title: 'Fetch user by id',
    description: 'Can be used to user by id from the database. Returns full data set of the user'} )
  @ApiOkResponse({ description: 'Returns user'} )
  @ApiResponse({ status: 401, description: 'Unauthorized'} )
  @Get(':id')
  async get(
    @Param('id') id): Promise<UserFullDto> {
    const user = await this.userService.findOneById(id);
    return this.userService.convertUserToFull(user);
  }

  /*
   * Update user by id
   */

  @ApiImplicitParam({
    name: 'id',
    description: 'Uid for the user',
    required: true })
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  @ApiOperation({
    title: 'Update user by id',
    description: 'Can be used to update user data. User can only change own data'} )
  @ApiOkResponse({ description: 'Returns updated user'} )
  @ApiForbiddenResponse( {description: 'Forbidden to use'} )
  @ApiResponse({ status: 401, description: 'Unauthorized'} )
  @Put(':id')
  async update(
    @Req() req,
    @Param('id') id,
    @Body(new ValidationPipe({transform: true})) updateUserDto: UpdateUserDto): Promise<UserSimpleDto> {
    if (req.user._id !== id) {
      throw new HttpException('Forbidden to use', 403);
    }
    const user: User = await this.userService.update(id, updateUserDto);
    return this.userService.convertUserToSimple(user);
  }

  /*
   * Delete user by id
   */

  @ApiImplicitParam({
    name: 'id',
    description: 'Uid for the user',
    required: true })
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({
    title: 'Delete user by id',
    description: 'Can be used to delete user data. User can only delete own user data'} )
  @ApiOkResponse({ description: 'Returns deleted user'} )
  @ApiForbiddenResponse( {description: 'Forbidden to use'} )
  @ApiResponse({ status: 401, description: 'Unauthorized'} )
  @Delete(':id')
  async delete(@Req() req, @Param('id') id): Promise<UserFullDto> {
    if (req.user._id !== id) {
      throw new HttpException('Forbidden to use', 403);
    }
    const user = await this.userService.delete(id);
    return this.userService.convertUserToFull(user);
  }

}
