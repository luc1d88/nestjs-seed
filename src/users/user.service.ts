import { Model } from 'mongoose';
import { Injectable, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interface/User';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserSimpleDto } from './dto/user-simple.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UserFullDto } from './dto/user-full.dto';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = await this.findOneByEmail(createUserDto.email);
    if (user) {
      throw new Error('User already exists');
    }
    const createdUser = new this.userModel(createUserDto);
    return createdUser.save();
  }

  findOneByEmail(email: string): Promise<User> {
    return this.userModel.findOne({email}).exec();
  }

  findOneById(id: string): Promise<User> {
    return this.userModel.findById(id).exec();
  }

  findAllByEmail(email: string): Promise<User[]> {
    const regex = new RegExp(email.toLowerCase().trim(), 'i');
    return this.userModel.find({email: { $regex: regex }} ).exec();
  }

  findAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  update(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    return this.userModel.findByIdAndUpdate(id, updateUserDto).exec();
  }

  delete(id: string): Promise<User> {
    return this.userModel.findByIdAndDelete(id).exec();
  }

  convertUserToSimple(userModel: User): UserSimpleDto {
    const userSimpleDto = new UserSimpleDto();
    userSimpleDto._id = userModel._id.toString();
    userSimpleDto.email = userModel.email;

    return userSimpleDto;
  }

  convertUserToFull(userModel: User): UserFullDto {
    const userFullDto = new UserFullDto();

    userFullDto._id = userModel._id.toString();
    userFullDto.firstName = userModel.firstName;
    userFullDto.lastName = userModel.lastName;
    userFullDto.email = userModel.email;

    return userFullDto;
  }
}
